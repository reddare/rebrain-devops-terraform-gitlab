provider "gitlab" {
    token = "${var.gitlab_token}"
}

resource "gitlab_project" "project" {
    name = "${var.gitlab_project}"
    visibility_level = "${var.gitlab_visibility_level}"
}

resource "gitlab_deploy_key" "deploy_key" {
  project = "${var.gitlab_username}/${var.gitlab_project}"
  title   = "${var.gitlab_deploy_key_name}"
  key     = "${var.gitlab_deploy_key}"
}
