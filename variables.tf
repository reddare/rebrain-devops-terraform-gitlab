variable "gitlab_username" {}
variable "gitlab_token" {}
variable "gitlab_project" {}
variable "gitlab_visibility_level" {}
variable "gitlab_deploy_key_name" {}
variable "gitlab_deploy_key" {}
